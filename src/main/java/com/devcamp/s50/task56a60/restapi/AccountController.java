package com.devcamp.s50.task56a60.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class AccountController {
     @GetMapping("/accounts")
     public ArrayList<Account> accounts() {
          Account account1 = new Account("1", "Tan Loc", 20);
          Account account2 = new Account("2", "My Cam", 30);
          Account account3 = new Account("3", "Phat Tai", 40);

          System.out.println(account1.toString());
          System.out.println(account2.toString());
          System.out.println(account3.toString());

          ArrayList<Account> accountsNew = new ArrayList<>();
          accountsNew.add(account1);
          accountsNew.add(account2);
          accountsNew.add(account3);

          return accountsNew;

     }
}
